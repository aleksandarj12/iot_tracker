//
//  Constants.swift
//  Swipe-Feature
//
//  Created by Aleksandar Jovanov on 11/18/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

let ENVIRONMENT = false

let mainBundle : Bundle = Bundle(identifier: "com.3pdevelopment.iotTracker")!
let googleMapsApiKey = "AIzaSyB1yGxEoQnfY4_wEcrG68LLWvm0GWWQELg"


#if(!ENVIRONMENT)
let allDevicesURL = "http://iot_test.virginmobile.sa/api/devices"
let sessionURL = "http://iot_test.virginmobile.sa/api/session"
#else
let allDevicesURL = "sdfdsfsd"
#endif


struct keys {
    static let failedGetResponse = "failed_get_response"
    static let successGetResponse = "success_get_response"
    static let commandResponseSuccess = "command_response_success"
    static let commandResponseFailed = "command_response_failed"
    static let updateDeviceInfoForIdNotification = "update_device_info_for_id"
    static let updateEventsInfoForIdNotification = "update_events_info_for_id"
    static let setPositionsNotificationCenter = "set_positions_center"
    static let updateEvents = "update_events"
    static let getDeviceMode = "get_device_mode"
}

struct localization {
    
    static let successfulActionTitle = "Successful Action"
    static let failedActionTitle = "Fail Action"
    
    struct deviceName {
        static let settingDeviceName = "Setting up device name…"
        static let failedDeviceName = "Your device name has not been successfully set. Please try again later."
        static let successfulDeviceName = "Your device name has been successfully set."
    }

    struct whiteList {
        static let settingWhiteList = "Setting up white list…"
        static let failedWhitelist = "Your white list has not been successfully set. Please try again later."
        static let successfulWhiteList = "Your white list has been successfully set."
    }
    struct oneTouchCall {
        static let settingOneTouchCall = "Setting up one-touch call…"
        static let successfulOneTouchCall = "Your one touch call has been successfully set."
        static let failedOneTouchCall = "Your one touch call has not been successfully set. Please try again later."

    }
    struct deviceMode {
        static let settingDeviceMode = "Setting up device mode…"
        static let failedDeviceMode = "Your device mode has not been successfully set. Please try again later."
        static let successfulDeviceMode = "Your device mode has been successfully set."
    }

}

