//
//  CustomSlider.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/7/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

protocol SliderDelegate {
    func didChangeValue(value : Int)
}
class CustomSlider: UISlider {

    private var values : [Int]!
//    private var lastIndex: Int? = nil
//    var delegate : SliderDelegate!
    

//    @objc func handleValueChange(sender: UISlider) {
//        let newIndex = Int(sender.value + 0.5) // round up to next index
//        self.setValue(Float(newIndex), animated: false) // snap to increments
//        let didChange = lastIndex == nil || newIndex != lastIndex!
//        if didChange {
//            let actualValue = self.values[newIndex]
//            self.value = Float(actualValue)
//            delegate.didChangeValue(value: actualValue)
//        }
//    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        
        //keeps original origin and width, changes height, you get the idea
        let customBounds = CGRect(origin: bounds.origin, size: CGSize(width: bounds.size.width, height: 10.0))
        super.trackRect(forBounds: customBounds)
        return customBounds
    }
    
    override func awakeFromNib() {
        self.setThumbImage(UIImage(named: "Oval"), for: .normal)
        super.awakeFromNib()
//        self.values = setNumbers()
        
        
        
//        self.minimumValue = 1
//        self.maximumValue = 152
    }

    


}
