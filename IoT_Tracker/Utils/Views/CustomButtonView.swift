//
//  CustomButtonView.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/7/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import GoogleMaps

protocol CustomButtonDelegate {
    func didSelectSettings()
    func didSelectAlerts()
    func didSelectCall()
    func didSelectNavigate()
}

class CustomButtonView: UIView {
    
    var spaceBetweenItems : CGFloat = 48
    var padding : CGFloat = 0
    var itemWidth : CGFloat = 55
    var numberOfSpaces : CGFloat = 3
    var numberOfItems : CGFloat = 4
    
    var delegate : CustomButtonDelegate!
    var option : ButtonOption!
    var posY : CGFloat!
    var availableWidth : CGFloat!
    
    init(_ option : ButtonOption, _ posY : CGFloat, _ availableWidth : CGFloat)
    {
        super.init(frame: CGRect.zero)
        self.option = option
        self.posY = posY
        self.availableWidth = availableWidth
        
        self.frame.origin.y = posY
        self.frame.size = CGSize(width: 55.0, height: 55.0)
        self.spaceBetweenItems = checkForAvailableSpaceBetween()
        let imageView : UIImageView = UIImageView()
        imageView.frame.size = CGSize(width: 38.0, height: 38.0)
        imageView.frame.origin.x = (self.frame.size.width / 2) - (imageView.frame.size.width / 2)
        imageView.frame.origin.y = 0.0
        
        let label : UILabel = UILabel()
        label.frame.size = CGSize(width: 58, height: 18)
        label.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        label.numberOfLines = 1
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.textColor = color.primaryMainRed
        label.frame.origin = CGPoint(x: 0.0, y: 39.0)
        
        let posX = (availableWidth - (itemWidth * numberOfItems + spaceBetweenItems * numberOfSpaces)) / 2
        
        switch option {
        case .alert:
            
            let image : UIImage = UIImage(named: "btnAlerts")!
            imageView.image = image
            self.frame.origin.x = posX + spaceBetweenItems + itemWidth
            let gesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(alertsButtonTapped))
            gesture.numberOfTapsRequired = 1
            self.isUserInteractionEnabled = true
            self.addGestureRecognizer(gesture)
            
            label.text = "Alerts"
            
            self.addSubview(imageView)
            self.addSubview(label)
            
        case .call:
            
            let image : UIImage = UIImage(named: "btnCall")!
            
            self.frame.origin.x = posX
            imageView.image = image
            
            let gesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(callButtonTapped))
            gesture.numberOfTapsRequired = 1
            self.isUserInteractionEnabled = true
            self.addGestureRecognizer(gesture)
            
            label.text = "Call"
            
            self.addSubview(imageView)
            self.addSubview(label)
            
            
        case .navigate:
            
            self.frame.origin.x = posX + (2 * spaceBetweenItems) + (2 * itemWidth)
            let image : UIImage = UIImage(named: "btnDirections")!
            imageView.image = image
            
            let gesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(navigateButtonTapped))
            gesture.numberOfTapsRequired = 1
            self.isUserInteractionEnabled = true
            self.addGestureRecognizer(gesture)
            
            label.text = "Navigate"
            
            self.addSubview(imageView)
            self.addSubview(label)
            
        case .settings:
            
            self.frame.origin.x = posX + (3 * spaceBetweenItems) + (3 * itemWidth)
            let image : UIImage = UIImage(named: "btnSettings")!
            imageView.image = image
            
            let gesture : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(settingsButtonTapped))
            gesture.numberOfTapsRequired = 1
            self.isUserInteractionEnabled = true
            self.addGestureRecognizer(gesture)
            
            label.text = "Settings"
            
            self.addSubview(imageView)
            self.addSubview(label)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func settingsButtonTapped()
    {
        self.delegate.didSelectSettings()
    }
    
    @objc func alertsButtonTapped()
    {
        self.delegate.didSelectAlerts()
    }
    
    @objc func callButtonTapped()
    {
        self.delegate.didSelectCall()
    }
    
    @objc func navigateButtonTapped()
    {
        self.delegate.didSelectNavigate()
    }

    
    func checkForAvailableSpaceBetween() -> CGFloat
    {
        if(UIScreen.main.bounds.size.width == 320)
        {
            return 20
        }
        else if(UIScreen.main.bounds.size.width == 375)
        {
            return 35
        }
        else
        {
            return 48
        }
    }
}
