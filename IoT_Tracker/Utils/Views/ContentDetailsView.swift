//
//  ContentDetailsView.swift
//  ScrollSwipe
//
//  Created by Aleksandar Jovanov on 11/26/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit
import GoogleMaps

enum DeviceStatus {
    case online
    case offline
    case unknown
}

enum ButtonOption {
    case alert
    case call
    case navigate
    case settings
}

protocol ContentDetailsDelegate {
    func didSelectSettings()
    func didSelectAlerts()
    func didSelectCall()
    func didSelectNavigate(coordinates : CLLocationCoordinate2D?)
}

final class ContentDetailsView: UIView, CustomButtonDelegate {
    
    var delegate : ContentDetailsDelegate!
    
    var leftPosX : CGFloat = 18.0
    var rightPosX : CGFloat!
    var originX : CGFloat = 0.0
    var deviceId : NSNumber?
    var extendImageView : UIImageView = UIImageView()
    var mainBatteryImageView : UIImageView = UIImageView()
    var userInfoView : UserInfoView = UserInfoView()
    var position : Position!
    var nameLabel : UILabel!
    var statusLabel : UILabel!
    var locationDescriptionLabel : UILabel!
    var periodAgoLabel : UILabel!
    var mainPercentageBatteryLabel : UILabel!
    var settingsButton : UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupProperties()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupProperties()
    {
        rightPosX = self.frame.size.width - 18
        self.setCornerRadiusView(radius: 7.0)
        let extendImage = UIImage(named: "btnExtendCard")
        extendImageView.frame.size = CGSize(width: (extendImage?.size.width)!, height: (extendImage?.size.height)!)
        extendImageView.image = extendImage
        extendImageView.frame.origin.y = 10
        extendImageView.center.x = self.frame.size.width / 2
        self.layer.borderColor = color.borderShadowColor.cgColor
        self.layer.borderWidth = 1.0
        nameLabel = UILabel(frame: CGRect(x: leftPosX, y: 28.0, width: 200.0, height: 20.0))
        nameLabel.textColor = color.mainTextColor
        nameLabel.textAlignment = .left
        nameLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
//        self.layer.shadowColor = color.shadowInfoColor.cgColor
//        self.layer.shadowOffset = CGSize(width: 5, height: 5)
//        self.layer.shadowOpacity = 1.0
//        self.layer.shadowRadius = 1.0
//        self.layer.masksToBounds = false
        
        statusLabel = UILabel(frame: CGRect(x: leftPosX, y: nameLabel.frame.size.height + nameLabel.frame.origin.y + 10, width: 60.0, height: 20.0))
        statusLabel.textColor = color.primaryStatusGreen
        statusLabel.textAlignment = .left
        statusLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        
        let mainBatteryImage = UIImage(named: "iconBattery")
        mainBatteryImageView.frame.size = CGSize(width: (mainBatteryImage?.size.width)!, height: (mainBatteryImage?.size.height)!)
        mainBatteryImageView.image = mainBatteryImage
        mainBatteryImageView.center.y = statusLabel.center.y
        mainBatteryImageView.frame.origin.x = statusLabel.frame.origin.x + statusLabel.frame.size.width
        
        mainPercentageBatteryLabel = UILabel()
        mainPercentageBatteryLabel.frame.size = CGSize(width: 40, height: 15)
        mainPercentageBatteryLabel.frame.origin.x = mainBatteryImageView.frame.origin.x + mainBatteryImageView.frame.size.width + 7
        mainPercentageBatteryLabel.center.y = statusLabel.center.y
        
        mainPercentageBatteryLabel.textColor = color.mainTextColor
        mainPercentageBatteryLabel.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        mainPercentageBatteryLabel.textAlignment = .left
        
        let lastUpdatedLocationLabel : UILabel = UILabel()
        lastUpdatedLocationLabel.frame.size = CGSize(width: 155, height: 17)
        lastUpdatedLocationLabel.textAlignment = .left
        lastUpdatedLocationLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        lastUpdatedLocationLabel.textColor = color.mainTextColor
        lastUpdatedLocationLabel.frame.origin = CGPoint(x: leftPosX, y: statusLabel.frame.size.height + statusLabel.frame.origin.y + 39)
        lastUpdatedLocationLabel.text = "Last Updated Location"
        
        
        locationDescriptionLabel = UILabel()
        locationDescriptionLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        locationDescriptionLabel.textColor = color.mainTextColor
        
        locationDescriptionLabel.frame.size.width = self.frame.size.width - 36
        locationDescriptionLabel.frame.size.height = locationDescriptionLabel.requiredHeight
        locationDescriptionLabel.frame.origin = CGPoint(x: 18.0, y: lastUpdatedLocationLabel.frame.size.height + lastUpdatedLocationLabel.frame.origin.y + 7)
        locationDescriptionLabel.textAlignment = .left
        locationDescriptionLabel.numberOfLines = 0
        
        periodAgoLabel = UILabel()
        periodAgoLabel.textColor = color.periodAgoColor
        periodAgoLabel.font = UIFont.systemFont(ofSize: 12.0, weight: .regular)
        periodAgoLabel.textAlignment = .left
        periodAgoLabel.frame.size = CGSize(width: 120.0, height: 15)
        periodAgoLabel.frame.origin = CGPoint(x: leftPosX, y: locationDescriptionLabel.frame.size.height + locationDescriptionLabel.frame.origin.y + 5)
        
//        settingsButton = UIButton(frame: CGRect(x: self.frame.maxX - 100, y: 40, width: 70, height: 30))
//        settingsButton.setTitle("Settings", for: .normal)
//        settingsButton.addTarget(self, action: #selector(settingsButtonTapped), for: .touchUpInside)
//        settingsButton.backgroundColor = color.primaryMainRed
//        settingsButton.layer.cornerRadius = 4.0
//        settingsButton.clipsToBounds = true
        
//        addSubview(settingsButton)
        addSubview(extendImageView)
        addSubview(nameLabel)
        addSubview(statusLabel)
        addSubview(mainBatteryImageView)
        addSubview(mainPercentageBatteryLabel)
        addSubview(setupShadowBorder(yPos: statusLabel.frame.size.height + statusLabel.frame.origin.y + 18))
        addSubview(lastUpdatedLocationLabel)
        addSubview(locationDescriptionLabel)
        addSubview(periodAgoLabel)
        addSubview(setupShadowBorder(yPos: periodAgoLabel.frame.size.height + periodAgoLabel.frame.origin.y + 14))
        
        let alertButtonView : CustomButtonView = CustomButtonView(.alert, (self.subviews.last?.frame.origin.y)! + 1 + 14, self.frame.size.width)
        alertButtonView.delegate = self
        addSubview(alertButtonView)
        
        let callButtonView : CustomButtonView = CustomButtonView(.call, (self.subviews.last?.frame.origin.y)!, self.frame.size.width)
        callButtonView.delegate = self
        addSubview(callButtonView)
        
        let navigateButtonView : CustomButtonView = CustomButtonView(.navigate, (self.subviews.last?.frame.origin.y)!, self.frame.size.width)
        navigateButtonView.delegate = self
        addSubview(navigateButtonView)
        
        let settingsButtonView : CustomButtonView = CustomButtonView(.settings, (self.subviews.last?.frame.origin.y)!, self.frame.size.width)
        settingsButtonView.delegate = self
        addSubview(settingsButtonView)
        
        addSubview(setupShadowBorder(yPos: (self.subviews.last?.frame.origin.y)! + (self.subviews.last?.frame.size.height)! + 14))
        userInfoView = UserInfoView(frame: CGRect(x: 22.0, y: (subviews.last?.frame.origin.y)! + 20, width: frame.size.width - 47.0, height: 300.0))
        
        addSubview(userInfoView)
    }
    
    func setData()
    {
        if let name = position.device.name {
            setNameLabel(name)
        }
        else
        {
            setNameLabel("John Doe")
        }
        
        setStatus((deviceStatus(status: position.device.status)))
        
        if let battery = position.batteryLevel {
            setMainPercentageBattery((battery.intValue.description) + " %")
        }
        else
        {
            setMainPercentageBattery("n/a")
        }
        
        setLocationDescription("University of Wollongong, Dubai International City, United Arab Emirates")
        setPeriodAgo("2 minutes ago")
        
        if let speed = position.speed {
            userInfoView.setSpeed(speed.description + " km/h")
        }
        else
        {
            userInfoView.setSpeed("0.00 km/h")
        }
        
        userInfoView.setLongitude((position.longitude?.description)!)
        userInfoView.setLatitude((position.latitude?.description)!)
        
        if let imei = position.device.uniqueId {
            userInfoView.setImei(imei)
        }
        else
        {
            userInfoView.setImei("13001572")
        }
        
    }
    
    func deviceStatus(status : DeviceStatus) -> String
    {
        switch status {
        case .offline:
            statusLabel.textColor = color.primaryMainRed
            return "Offline"
        case .online:
            statusLabel.textColor = color.primaryStatusGreen
            return "Online"
        case .unknown:
            statusLabel.textColor = color.primaryMainRed
            return "Unknown"
        }
    }
    
    //MARK: Set Methods
    
    func setPeriodAgo(_ period : String)
    {
        periodAgoLabel.text = period
    }
    
    func setNameLabel(_ name : String)
    {
        self.nameLabel.text = name
    }
   
    func setStatus(_ status : String)
    {
        statusLabel.text = status
    }
    
    func setMainPercentageBattery(_ percentage : String)
    {
        mainPercentageBatteryLabel.text = percentage
    }
    
    func setLocationDescription(_ address : String)
    {
        locationDescriptionLabel.text = address
    }
    
    //MARK: Setup & Support Methods
    
    
    
    func setupShadowBorder(yPos : CGFloat) -> UIView
    {
        let view = UIView()
        view.frame.size = CGSize(width: self.frame.size.width - 36, height: 1)
        view.backgroundColor = color.borderShadowColor
        view.frame.origin.x = 18
        view.frame.origin.y = yPos
        
        return view
    }
    
    
    //MARK: CustomButtonDelegate
    
    func didSelectSettings()
    {
        DataManager.sharedInstance.setSelectedPosition(self.position)
        self.delegate.didSelectSettings()
    }
    
    func didSelectAlerts()
    {
        self.delegate.didSelectAlerts()
    }
    
    func didSelectCall()
    {
        self.delegate.didSelectCall()
    }
    
    func didSelectNavigate()
    {
        let coordinates : CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: position.latitude!)!, longitude: CLLocationDegrees(exactly: position.longitude!)!)
        
        self.delegate.didSelectNavigate(coordinates: coordinates)
    }
    
}

extension UILabel {
    
public var requiredHeight: CGFloat {
    let label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.attributedText = attributedText
    label.sizeToFit()
    if((label.frame.height + 2) < 34)
    {
        return 34
    }
    return label.frame.height + 2
}
}
