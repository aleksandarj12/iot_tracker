//
//  DimmedView.swift
//  Swipe-Feature
//
//  Created by Aleksandar Jovanov on 11/19/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class DimmedView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.2)        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}

extension UIView
{
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
    
    func createCopy(myView : UIView) -> UIView
    {
        let archive = NSKeyedArchiver.archivedData(withRootObject: myView)

        let myViewCopy = NSKeyedUnarchiver.unarchiveObject(with: archive) as! UIView

        return myViewCopy
    }
}
