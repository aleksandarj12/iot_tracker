//
//  UserInfoView.swift
//  ScrollSwipe
//
//  Created by Aleksandar Jovanov on 11/27/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class UserInfoView: UIView {

    var speedLabel : DetailsInfoLabel = DetailsInfoLabel()
    var longitudeLabel : DetailsInfoLabel = DetailsInfoLabel()
    var latitudeLabel : DetailsInfoLabel = DetailsInfoLabel()
    var imeiLabel : DetailsInfoLabel = DetailsInfoLabel()
    
    var numberOfElements : [InfoDetailsDevice] = [InfoDetailsDevice]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupInfoDetailsDetails()
        setupProperties()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    func setupProperties()
    {
        self.setCornerRadiusView(radius: 7.0)
        var yPos : CGFloat = 0.0
        var leftLabel : UILabel = UILabel()
        leftLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        leftLabel.textColor = color.mainTextColor
        leftLabel.textAlignment = .left
        
        for info in numberOfElements
        {
            let view : UIView = UIView(frame: CGRect(x: 0, y: yPos, width: self.frame.size.width, height: 30.0))
           
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            imageView.image = info.image
            
            leftLabel = UILabel(frame: CGRect(x: imageView.frame.size.width + 15, y: 0, width: 130.0, height: 20.0))
            leftLabel.text = info.nameLabel
            leftLabel.center.y = imageView.center.y

            info.label = DetailsInfoLabel(frame: CGRect(x: self.frame.size.width - 160.0 + 15, y: 0, width: 140.0, height: 20.0))
            info.label?.center.y = imageView.center.y
            
            view.addSubview(info.label!)
            view.addSubview(imageView)
            view.addSubview(leftLabel)
            
            addSubview(view)
            
            yPos += 60
        }
        
        self.speedLabel = numberOfElements[0].label!
        self.longitudeLabel = numberOfElements[1].label!
        self.latitudeLabel = numberOfElements[2].label!
        self.imeiLabel = numberOfElements[3].label!
    }
    
    func setupInfoDetailsDetails()
    {
        let speed = InfoDetailsDevice()
        speed.image = UIImage(named: "iconSpeedDetails")!
        speed.nameLabel = "Speed"
        speed.label = self.speedLabel
        
        let longitude = InfoDetailsDevice()
        longitude.image = UIImage(named: "iconLongitudeDetails")!
        longitude.nameLabel = "Longitude"
        longitude.label = self.longitudeLabel
        
        let latitude = InfoDetailsDevice()
        latitude.image = UIImage(named: "iconLatitudeDetails")!
        latitude.nameLabel = "Latitude"
        latitude.label = self.latitudeLabel
        
        let imei = InfoDetailsDevice()
        imei.image = UIImage(named: "iconDeviceImei")!
        imei.nameLabel = "IMEI"
        imei.label = self.imeiLabel
        
        numberOfElements = [speed, longitude, latitude, imei]
    }
    
    func setSpeed(_ speed : String)
    {
        self.speedLabel.text = speed
    }
    
    func setLongitude(_ long : String)
    {
        self.longitudeLabel.text = long
    }
    
    func setLatitude(_ lat : String)
    {
        self.latitudeLabel.text = lat
    }
    
    func setImei(_ imei : String)
    {
        self.imeiLabel.text = imei
    }
    
}

class InfoDetailsDevice: NSObject {
    var image : UIImage = UIImage()
    var nameLabel : String?
    var label : DetailsInfoLabel?
}

class DetailsInfoLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLabel()
    {
        self.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        self.textColor = color.rightDetailsColor
        self.textAlignment = .right
        
    }
    
}
extension UIView {
    
    func setCornerRadiusView(radius : CGFloat)
    {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
}
