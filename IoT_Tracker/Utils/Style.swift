//
//  Style.swift
//  IoT_SDK
//
//  Created by Aleksandar Jovanov on 11/13/18.
//

import UIKit

struct color {
    static let primaryMainRed : UIColor = UIColor(red:0.84, green:0.01, blue:0.23, alpha:1.0)
    static let primaryStatusGreen = UIColor(red:0.12, green:0.72, blue:0.52, alpha:1.0)
    static let mainTextColor = UIColor(red:0.25, green:0.25, blue:0.25, alpha:1.0)
    static let borderShadowColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0)
    static let periodAgoColor = UIColor(red:0.69, green:0.69, blue:0.69, alpha:1.0)
    static let rightDetailsColor = UIColor(red:0.64, green:0.64, blue:0.64, alpha:1.0)
    static let borderColor = UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.0)
    static let shadowInfoColor = UIColor(red:0.26, green:0.29, blue:0.42, alpha:0.2)
    
    struct table {
        static let sectionBackgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
        static let sectionTextColor = UIColor(red:0.64, green:0.64, blue:0.64, alpha:1.0)
        static let separatorColor = UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
    }
    struct textField {
        static let borderColor = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
    }
}

struct font {
    static let systemFontMedium : UIFont = UIFont.systemFont(ofSize: 14.0, weight: .medium)
    static let systemFontRegular : UIFont = UIFont.systemFont(ofSize: 14.0, weight: .regular)
}

struct titleTextAttribute {
    static let primaryRegularTitle = [NSAttributedString.Key.font : font.systemFontMedium]
}
