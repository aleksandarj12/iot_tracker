//
//  SupportFunctions.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 11/21/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

class SupportFunctions: NSObject {
    
    class func JSONObjectToJSONString(_ jsonObject : [String: Any]) -> String
    {
        let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        return String(data: jsonData, encoding: String.Encoding.utf8)!
    }
    
    class func removeSpacePhoneNumbers(_ numbers : [PhoneNumber], _ option : CallOption) -> String
    {
        var numbersString : String = ""
        
        for number in numbers
        {
            let trimmedString = String(number.number.filter { !" ".contains($0) })
            
            numbersString.append(trimmedString)
            numbersString.append(",")
        }
        numbersString.removeLast()
        
        if(option == .oneTouchList)
        {
            if(numbers.count != 3)
            {
                var numbersOfComma = 3 - numbers.count
                while(numbersOfComma != 0)
                {
                    numbersString.append(",")
                    numbersOfComma -= 1
                }
            }
        }
        else if(option == .whiteList)
        {
            if(numbers.count != 15)
            {
                var numberOfComma = 15 - numbers.count
                while(numberOfComma != 0)
                {
                    numbersString.append(",")
                    numberOfComma -= 1
                }
            }
        }
        
        
        
        numbersString.append("#")
        
        return numbersString
    }
}
