//
//  ValidationControl.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/6/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

class ValidationControl: NSObject {

    class func validateNumber(number : String) -> Bool
    {
        if(number.count == 12)
        {
            let first3 = number.prefix(3)
            if(first3 == "057")
            {
                return true
            }
        }
        else if(number.count == 15)
        {
            let first3 = number.prefix(3)
            let second2 : NSString = (number as NSString).substring(with: NSRange(location: 4, length: 2)) as NSString
            if(first3 == "966" && second2 == "57")
            {
                return true
            }
        }
        else if(number.count == 16)
        {
            let first3 = number.prefix(4)
            let second2 : NSString = (number as NSString).substring(with: NSRange(location: 5, length: 2)) as NSString
            if(first3 == "+966" && second2 == "57")
            {
                return true
            }
        }
        return false
    }
    
    class func checkForStringParams(_ number : String, _ character : String) -> String
    {
        var newString : String = number
        var numberCounter : Int = 12
        
        if(character == "")
        {
            newString.removeLast()
            if(newString.last == " ")
            {
                newString.removeLast()
            }
        }
        else
        {
            if(newString.first == "+")
            {
                numberCounter = 16
                if(newString.count == 4 || newString.count == 7 || newString.count == 11)
                {
                    newString.append(" ")
                }
            }
            else
            {
                if((newString.count == 3 || newString.count == 7) && newString.first != "9")
                {
                    newString.append(" ")
                }
                else if((newString.count == 3 || newString.count == 6 || newString.count == 10) && newString.first == "9")
                {
                    newString.append(" ")
                }
            }
            
            if(newString.first == "9")
            {
                numberCounter = 15
            }
            
            if(numberCounter > newString.count)
            {
                newString.append(character)
            }
        }
        
        return newString
    }
}
