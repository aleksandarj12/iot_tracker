//
//  MainTestViewController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 11/21/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import GoogleMaps
import MBProgressHUD
import Alamofire

enum SwipePositions
{
    case tip
    case half
    case full
}

class DevicesViewController: BaseViewController, GMSMapViewDelegate, DeviceListView, UIScrollViewDelegate {

    @IBOutlet var sateliteButton: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    private var indexOfCellBeforeDragging = 0
    @IBOutlet var mapView: GMSMapView!
 
    var locationManager = CLLocationManager()
    var locationFlag : Bool = true
    var selectedCoordinates : CLLocationCoordinate2D!
    
    @IBOutlet var shadowView: UIView!
    var velocity : CGFloat = 15.0
    var currentMovePosition : SwipePositions = .tip
    @IBOutlet var navigateLabel: UILabel!
    var positions : NSMutableDictionary = NSMutableDictionary()
    var cellHeight : CGFloat = 180.0
    var cellWidth : CGFloat = UIScreen.main.bounds.size.width - 30
    var presenter = DeviceListPresenter()
    var cellIndex : Int = 0
    var markers : [GMSMarker] = [GMSMarker]()
    var maxTopMove : CGFloat = 0.0
    var maxBotMove : CGFloat = 0.0
    var baseY : CGFloat = 0.0

    //MARK: Lifecycle & Init
    
    override func viewWillAppear(_ animated: Bool)
    {
        if(currentMovePosition == .full || currentMovePosition == .half)
        {
            changeBackgroundAlpha(0.2)
        }
        super.viewWillAppear(animated)
        self.setupRightNavigation()
        setTitle("Devices Map")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
//        SocketManager.sharedInstance.closeSocket()
        //tuka
        //socket not closed...wonder when....
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.setupMapView()
        setupVerticalSwipe(scrollView)
        
        setupDimmedBackgroundView()

        presenter.attachView(view: self)
        presenter.getAllDevices()

    }
    
//    public init(_ nibNameString : String, _ bundle : Bundle?) {
//        super.init(nibName: nibNameString, bundle: bundle);
//    }
//    
//    required public init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    

    
    
    //MARK: DeviceListView
    
    func updateDeviceInfoForId(_ id: NSNumber)
    {
        for view in scrollView.subviews
        {
            let positionView = view as? ContentDetailsView
            if(positionView?.deviceId == id)
            {
                positionView?.position = DataManager.sharedInstance.getAllPositions().value(forKey: id.description) as? Position
                positionView?.setData()
            }
        }
        
    }

    
    func updateMapView(_ position : Position)
    {
        //TUKA
        //update with animation not good
        self.mapView.animate(to: GMSCameraPosition(target: CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: position.latitude!)!, longitude: CLLocationDegrees(exactly: position.longitude!)!), zoom: mapView.camera.zoom, bearing: 0, viewingAngle: 0))
    }
    
    func setPositions(id : NSNumber?)
    {
        self.positions = DataManager.sharedInstance.getAllPositions()
        
        if let deviceId = id
        {
            updateScrollView(deviceId)
        }
        else
        {
            setupScrollView()
        }
    
        self.drawMapView()
    }
    
    
    //MARK: Vertical Scroll
    
    //When moving toward top, we are calculating the difference
    
    func calculateMovement(_ gesture : UIPanGestureRecognizer, _ isMovingUp : Bool)
    {
        if(currentMovePosition == .tip && isMovingUp)
        {
            let currentPositionDiff = (resizeViewFor(.half) - resizeViewFor(.tip)) / 2
            if(gesture.view!.frame.size.height - resizeViewFor(.tip) > currentPositionDiff)
            {
                gesture.state = .ended
                moveWithVelocity(gesture, isMovingUp)
                self.currentMovePosition = .half
                shadowView.alpha = 0.0
                changeBackgroundAlpha(0.2)
            }
            
        }
        else if(currentMovePosition == .half && isMovingUp)
        {
            let currentPositionDiff = (resizeViewFor(.full) - resizeViewFor(.half)) / 2
            if((gesture.view?.frame.size.height)! - resizeViewFor(.half) > currentPositionDiff)
            {
                gesture.state = .ended
                moveWithVelocity(gesture, isMovingUp)
                self.currentMovePosition = .full
                shadowView.alpha = 0.0
            }
        }
        else if(currentMovePosition == .full && !isMovingUp)
        {
            let currentPositionDiff = (resizeViewFor(.full) - resizeViewFor(.half)) / 2
            if(gesture.view!.frame.size.height - resizeViewFor(.half) < currentPositionDiff)
            {
                gesture.state = .ended
                moveWithVelocity(gesture, isMovingUp)
                self.currentMovePosition = .half
                shadowView.alpha = 0.0
                changeBackgroundAlpha(0.2)
            }
        }
        else if(currentMovePosition == .half && !isMovingUp)
        {
            let currentPositionDiff = (resizeViewFor(.half) - resizeViewFor(.tip)) / 2
            if((gesture.view?.frame.size.height)! - resizeViewFor(.tip) < currentPositionDiff)
            {
                moveWithVelocity(gesture, isMovingUp)
                self.currentMovePosition = .tip
                shadowView.alpha = 1.0
                changeBackgroundAlpha(0.0)
                enableHorizontalScroll(true)
            }
        }
        
    }
    
    
    func resizeViewFor(_ position : SwipePositions) -> CGFloat
    {
        switch position {
        case .tip:
            return 210
        case .half:
            return 290
        case .full:
            return 540
        }
    }
    
    func moveYPosition(_ position : SwipePositions) -> CGFloat
    {
        switch position {
        case .tip:
            return 210
        case .half:
            return self.baseY - (self.resizeViewFor(.half) - self.resizeViewFor(.tip))
        case .full:
            return 540
        }
    }
    
    @objc func wasBasicDragged(gestureRecognizer: UIPanGestureRecognizer) {
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            let translation = gestureRecognizer.translation(in: self.view)
            
            if(translation.y < 0)
            {
                //UP
                enableHorizontalScroll(false)
                if(abs(translation.y) > velocity)
                {
                    gestureRecognizer.state = .ended
                    moveWithVelocity(gestureRecognizer, true)
                }
                else if(scrollView.frame.origin.y > maxTopMove)
                {
                    UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .allowAnimatedContent, animations:{
                        gestureRecognizer.view?.frame.size.height -= translation.y
                        gestureRecognizer.view?.subviews[self.cellIndex].frame.size.height -= translation.y
                        gestureRecognizer.view?.frame.origin.y -= abs(translation.y)
                        
                    }) { (flag) in
                        if(self.currentMovePosition == .tip && (gestureRecognizer.view?.frame.size.height)! > self.resizeViewFor(.half))
                        {
                            self.currentMovePosition = .half
                            self.shadowView.alpha = 0.0
                            self.changeBackgroundAlpha(0.2)
                        }
                        else
                        {
                            self.calculateMovement(gestureRecognizer, true)
                        }
                    }
                }
            }
            else
            {
                //DOWN
                if(scrollView.frame.origin.y < maxBotMove)
                {
                    if(translation.y > velocity)
                    {
                        gestureRecognizer.state = .ended
                        moveWithVelocity(gestureRecognizer, false)
                        
                    }
                        //resize -->
                    else if(translation.y > 0 && scrollView.frame.size.height > self.resizeViewFor(.tip))
                    {
                        //                        print(translation.y)
                        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .allowAnimatedContent, animations:{
                            gestureRecognizer.view?.frame.size.height -= translation.y
                            gestureRecognizer.view?.subviews[self.cellIndex].frame.size.height -= translation.y
                            //move -◊Ï->
                            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                            //<--move
                        }) { (flag) in
                            self.calculateMovement(gestureRecognizer, false)
                        }
                    }
                }
            }
            gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
        }
    }
    
    func moveWithVelocity(_ gesture : UIPanGestureRecognizer, _ isUp : Bool)
    {
        if(isUp)
        {
            if(self.currentMovePosition == .tip)
            {
                self.currentMovePosition = .half
                shadowView.alpha = 0.0
                changeBackgroundAlpha(0.2)
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .allowAnimatedContent, animations:{
                    self.scrollView.frame.origin.y -= abs((self.resizeViewFor(.half) - (gesture.view?.frame.size.height)!))
                    
                    self.scrollView.frame.size.height = self.resizeViewFor(.half)
                    self.scrollView.subviews[self.cellIndex].frame.size.height = self.resizeViewFor(.half)
                }, completion: nil)
            }
            else if(self.currentMovePosition == .half)
            {
                self.currentMovePosition = .full
                shadowView.alpha = 0.0
                
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .allowAnimatedContent, animations:{
                    
                    self.scrollView.frame.origin.y -= abs((self.resizeViewFor(.full) - (gesture.view?.frame.size.height)!))
                    self.scrollView.frame.size.height = self.resizeViewFor(.full)
                    self.scrollView.subviews[self.cellIndex].frame.size.height = self.resizeViewFor(.full)
                }, completion : nil)
            }
        }
        else
        {
            if(self.currentMovePosition == .full)
            {
                self.currentMovePosition = .half
                shadowView.alpha = 0.0
                changeBackgroundAlpha(0.2)
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .allowAnimatedContent, animations:{
                    self.scrollView.frame.origin.y -= (self.resizeViewFor(.half) - (gesture.view?.frame.size.height)!)
                    self.scrollView.frame.size.height = self.resizeViewFor(.half)
                    self.scrollView.subviews[self.cellIndex].frame.size.height = self.resizeViewFor(.half)
                }, completion: nil)
            }
            else if(self.currentMovePosition == .half)
            {
                self.currentMovePosition = .tip
                shadowView.alpha = 1.0
                changeBackgroundAlpha(0.0)
                enableHorizontalScroll(true)
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .allowAnimatedContent, animations:{
                    self.scrollView.frame.origin.y -= (self.resizeViewFor(.tip) - (gesture.view?.frame.size.height)!)
                    self.scrollView.frame.size.height = self.resizeViewFor(.tip)
                    self.scrollView.subviews[self.cellIndex].frame.size.height = self.resizeViewFor(.tip)
                }, completion: nil)
            }
        }
        
    }
    
    
    //MARK: Horizontal Scroll
    
    func checkGreaterThanHalf(scrolled : CGFloat) -> Bool
    {
        let multiplies = scrolled / cellWidth
        let fullNumber = Int(multiplies)
        if(multiplies - CGFloat(fullNumber) > 0.5)
        {
            cellIndex = fullNumber + 1
            return true
        }
        else
        {
            cellIndex = fullNumber
            return false
        }
    }
    
    func checkForSelection(scrolled : CGFloat)
    {
        let diff = scrolled - cellWidth
        
            if((cellWidth / 2) - cellWidth < diff)
            {
                if((CGFloat(cellIndex) * cellWidth) > scrolled)
                {
                    cellIndex -= 1
                }
                else
                {
                    cellIndex += 1
                }
            }
            else
            {
                cellIndex = 0
            }
        
       
        drawMapView()
    }


    //MARK: UISCrollView Delegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        cellIndex = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        drawMapView()
//        let position : Position = DataManager.sharedInstance.getAllPositions().allValues[cellIndex] as! Position
//        updateMapView(position)
    }
    
    
    //MARK: Setup & Support Methods
    
    func setupDimmedBackgroundView()
    {
        shadowView.backgroundColor = color.shadowInfoColor
        let view : UIView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        view.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
        view.tag = 999
        view.alpha = 0
        
        let navView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.frame.size.width)!, height: (self.navigationController?.navigationBar.frame.size.height)!))
        navView.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
        navView.tag = 998
        navView.alpha = 0
        
        self.view.insertSubview(view, belowSubview: scrollView)
        self.navigationController?.navigationBar.addSubview(navView)
    }
    
    func updateScrollView(_ id : NSNumber)
    {
        for view in scrollView.subviews as! [ContentDetailsView]
        {
            if(view.deviceId == id)
            {
                view.position = DataManager.sharedInstance.getAllPositions().value(forKey: id.description) as? Position
                view.setData()
                break
            }
        }
    }
    
    func setupScrollView()
    {
        scrollView.delegate = self
        maxTopMove = self.resizeViewFor(.full) - self.resizeViewFor(.half) - self.resizeViewFor(.tip) - 15
        maxBotMove = scrollView.frame.maxY
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.layer.cornerRadius = 10.0
        scrollView.clipsToBounds = true
        scrollView.backgroundColor = UIColor.clear
        baseY = scrollView.frame.origin.y
        scrollView.isPagingEnabled = true
        

        let padding : CGFloat = 17
        let viewWidth = scrollView.frame.size.width - 2 * padding
        
        var x : CGFloat = 0
        
        for position in positions.allValues as! [Position]
        {
            let view : ContentDetailsView = ContentDetailsView(frame: CGRect(x: x + padding, y: 0, width: viewWidth, height: 210.0))
            view.delegate = self
            view.position = position
            view.deviceId = position.deviceId
            view.setData()
            view.backgroundColor = UIColor.white
            
            scrollView.addSubview(view)
            
            
            x = view.frame.origin.x + viewWidth + padding
        }
        
        scrollView.contentSize = CGSize(width:x+padding, height:scrollView.frame.size.height)
        shadowView.alpha = 1.0
    }
    
    func checkSameCoordinates(_ index : Int, _ coordinates : CLLocationCoordinate2D) -> Bool
    {
        let position : Position = positions.allValues[index] as! Position
        if(CLLocationDegrees(exactly: position.longitude!) == coordinates.longitude && CLLocationDegrees(exactly: position.latitude!) == coordinates.latitude)
        {
            return true
        }
        return false
    }
    
    func changeBackgroundAlpha(_ alpha : CGFloat)
    {
        var contentView : UIView!
        var navView : UIView!
        
        for view in self.view.subviews
        {
            if(view.tag == 999)
            {
                contentView = view
            }
        }
        for view in (self.navigationController?.navigationBar.subviews)!
        {
            if(view.tag == 998)
            {
                navView = view
            }
        }
        
        UIView.animate(withDuration: 0.5) {
            contentView.alpha = alpha
            navView.alpha = alpha
            UtilityManager.sharedInstance.setStatusBarBackgroundColor(alpha)
        }
    }
    
    func loadingView(_ flag: Bool)
    {
        if(flag)
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        else
        {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func setupMarkerStyle(_ counter : Int) -> UIImage
    {
        if(cellIndex == counter)
        {
            return UIImage(named: "iconChildPinSelected")!
        }
        
        return UIImage(named: "iconChildPin")!
    }
    
    func setupMapView()
    {
        mapView.delegate = self
        mapView.setMinZoom(12, maxZoom: 24)
        do
        {
            //THIS IS FOR SDK
            //            let podBundle = Bundle(for: DeviceListViewController.self)
            //            let bundleURL = podBundle.url(forResource: "IoT_SDK", withExtension: "bundle")
            //            let bundle = Bundle(url: bundleURL!)
            
            
            //THIS IS FOR TRACKER
            
            //            let bundleURL = podBundle.url(forResource: "com.3pdevelopment.iotTracker", withExtension: "bundle")
            let bundle = Bundle(identifier: "com.3pdevelopment.iotTracker")
            
            if let styleURL = bundle?.url(forResource: "mapStyle", withExtension: "json")
            {
                self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            }
            else
            {
                NSLog("Unable to find style.json")
            }
        }
        catch
        {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
    }
    
    func setupVerticalSwipe(_ view : UIView)
    {
        let mainGesture = UIPanGestureRecognizer(target: self, action: #selector(wasBasicDragged(gestureRecognizer:)))
        view.addGestureRecognizer(mainGesture)
        view.isUserInteractionEnabled = true
        mainGesture.delegate = self
    }
    
    func setupRightNavigation()
    {
//        self.navigationItem.title = "Devices Map"
//        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : font.systemFontRegular, NSAttributedString.Key.foregroundColor: UIColor.white]
//        self.navigationController?.navigationBar.barTintColor = color.primaryMainRed
//        self.navigationController?.navigationBar.isTranslucent = false
        let rightBarButtonItem : UIBarButtonItem = UIBarButtonItem(title: "Help", style: .plain, target: self, action: nil)
        rightBarButtonItem.tintColor = UIColor.white
        rightBarButtonItem.setTitleTextAttributes(titleTextAttribute.primaryRegularTitle, for: .normal)
        rightBarButtonItem.setTitleTextAttributes(titleTextAttribute.primaryRegularTitle, for: .selected)
        rightBarButtonItem.setTitleTextAttributes(titleTextAttribute.primaryRegularTitle, for: .focused)
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    
    //MARK: GMSMapViewDelegate
    
    func drawMapView()
    {
        let selectedPosition = self.positions.allValues[cellIndex]
        
        updateMapView(selectedPosition as! Position)
        
        self.markers.removeAll()
        mapView.clear()
        
        var counter : Int = 0
        for position in self.positions.allValues as! [Position]
        {
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: position.latitude!)!, longitude: CLLocationDegrees(exactly: position.longitude!)!))
            marker.icon = setupMarkerStyle(counter)
            self.markers.append(marker)
            marker.map = mapView

            counter += 1
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        for i in 0...positions.count - 1
        {
            if(checkSameCoordinates(i, marker.position))
            {
                cellIndex = i
                drawMapView()
            }
        }
        
        scrollView.setContentOffset(CGPoint(x: (CGFloat(cellIndex) * scrollView.frame.size.width), y: scrollView.contentOffset.y), animated: true)
        
        return true
    }

    
    //MARK: Action Methods
    
    @IBAction func earthButtonTapped(_ sender: Any)
    {
        if(mapView.mapType == .normal)
        {
            mapView.mapType = .satellite
            sateliteButton.setImage(UIImage(named: "btnSatelite"), for: .normal)
        }
        else
        {
            mapView.mapType = .normal
            sateliteButton.setImage(UIImage(named: "btnEarth"), for: .normal)
        }
    }
    
    @IBAction func geoFencesButtonTapped(_ sender: Any)
    {
        
    }
    
    func enableHorizontalScroll(_ flag : Bool)
    {
        scrollView.isScrollEnabled = flag
        scrollView.isPagingEnabled = flag
    }
}

extension DevicesViewController : UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool
    {
        if let pan = gestureRecognizer as? UIPanGestureRecognizer
        {
            let velocity = pan.velocity(in: self.view)
            return abs(velocity.y) > abs(velocity.x)
        }
        return true
    }
    
}

extension DevicesViewController : ContentDetailsDelegate {
    
    func didSelectSettings()
    {
        let controller = SettingsOptionsViewController()
        changeBackgroundAlpha(0.0)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func didSelectAlerts() {
        
    }
    
    func didSelectCall() {
        
    }
    
    func didSelectNavigate(coordinates : CLLocationCoordinate2D?) {
        locationFlag = true
        if(coordinates?.latitude == nil || coordinates?.longitude == nil)
        {
            let controller : UIAlertController = UIAlertController(title: "", message: "Latitude or Longitude are not available", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(controller, animated: true, completion: nil)
        }
        else
        {
            self.selectedCoordinates = coordinates
            locationManager.delegate = self
            if(CLLocationManager.authorizationStatus() != .notDetermined)
            {
                locationManager.startUpdatingLocation()
            }
            else
            {
                locationManager.requestWhenInUseAuthorization()
            }
        }
    }
    
    
}


extension DevicesViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        if(CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
        {
            locationFlag = true
            locationManager.startUpdatingLocation()
        }
        else if(CLLocationManager.authorizationStatus() == .notDetermined)
        {
            locationManager.requestWhenInUseAuthorization()
        }
        else
        {
            let controller : UIAlertController = UIAlertController(title: "", message: "You need to allow location services. Change in Privacy.", preferredStyle: .alert)
            controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        locationManager.stopUpdatingLocation()
        if(locationFlag)
        {
            let lat = locations.first?.coordinate.latitude
            let long = locations.first?.coordinate.longitude
            
            locationFlag = false
            if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)
            {
                let destination : String = self.selectedCoordinates!.latitude.description+","+self.selectedCoordinates!.longitude.description
                let origin : String = (lat?.description)!+","+(long?.description)!
               
                
                let newUrl : String = "comgooglemaps://?saddr="+origin+"&daddr="+destination+"&directionsmode=driving"
                
                
                UIApplication.shared.open(URL(string: newUrl)!, options: [:], completionHandler: nil)
            }
            else
            {
                let controller : UIAlertController = UIAlertController(title: "", message: "Google Maps app not installed.", preferredStyle: .alert)
                controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(controller, animated: true, completion: nil)
            }

        }
    }
}
