//
//  DeviceListPresenter.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 11/20/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import Alamofire

class DeviceListPresenter: NSObject {
    
    
    var view : DeviceListView!
    
    func attachView(view: DeviceListView)
    {
        self.view = view
        setupNotificationCenter()
    }
    
    func detachView() -> Void
    {
        self.view = nil
    }
    
    func setupNotificationCenter()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(updateDeviceInfoForId(_:)), name: NSNotification.Name(rawValue: keys.updateDeviceInfoForIdNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setPositions(_:)), name: NSNotification.Name(rawValue: keys.setPositionsNotificationCenter), object: nil)
        
    }
    
    @objc func updateDeviceInfoForId(_ id : Notification)
    {
        self.view.updateDeviceInfoForId(id.userInfo?["id"] as! NSNumber)
    }
    
    @objc func setPositions(_ id : NSNumber?)
    {
        self.view.setPositions(id: nil)
    }
    
    func getAllDevices()
    {
        self.view.loadingView(true)
        
        let urlString = "http://iot_test.virginmobile.sa/api/session?token="+"VoVFR1GrTH1wFPsg8VcsrIjv5po0ifP1"

        Alamofire.request(urlString, method: .get, parameters: nil, headers: nil)
            .responseJSON { response in
                if(response.error == nil)
                {
                    let cookies = HTTPCookie.cookies(withResponseHeaderFields: response.response?.allHeaderFields as! [String : String] , for: (response.request?.url!)!)
                    
                    UtilityManager.sharedInstance.setCookie(((cookies as AnyObject).value(forKey: "value") as AnyObject).object(at: 0) as! String)
                    Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.setCookies(cookies, for: response.request?.url, mainDocumentURL: URL(string: "http://iot_test.virginmobile.sa/"))
                    self.getDevicesInfo()
                    print("successful")
                }
                else
                {
                    self.view.loadingView(false)
                    print("Not successful")
                }
        }
    }
    
    func getDevicesInfo()
    {
        let header : HTTPHeaders = [
            "Content-Type" : "application/json",
            "Cookie" : "JSESSIONID=" + UtilityManager.sharedInstance.getCookie()
        ]
        
        let url : String = "http://iot_test.virginmobile.sa/api/devices"
        Alamofire.request(url, method: .get, parameters: nil, headers: header)
            .responseJSON { response in
                self.view.loadingView(false)
                do {
                    let json = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                    
                    self.addDeviceInfoPosition(data: json)
                    SocketManager.sharedInstance.setupWebSocket()
                }
                catch {
                    print("error parsing JSON")
                }
        }
    }
    
    private func addDeviceInfoPosition(data : AnyObject)
    {
        for device in data as! NSArray
        {
            DataManager.sharedInstance.updateDeviceInfoForPosition(device as! [String : AnyObject])
        }
        
    }
}
