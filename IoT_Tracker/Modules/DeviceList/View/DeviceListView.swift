//
//  DeviceListView.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 11/20/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

protocol DeviceListView {
    func setPositions(id : NSNumber?)
    func loadingView(_ flag : Bool)
    func updateDeviceInfoForId(_ id : NSNumber)
}
