//
//  DataManager.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 11/19/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import Alamofire

class DataManager: NSObject {
    
    var allPositions: NSMutableDictionary = NSMutableDictionary()
    var allDevices: NSMutableDictionary = NSMutableDictionary()

    var devices: [Device] {
        get {
            return allDevices.allValues as! [Device]
        }
    }
    
    static let sharedInstance : DataManager = {
        let instance = DataManager()
        return instance
    }()
    
    func positionByDeviceId(_ deviceId: NSNumber) -> Position? {
        if let p = allPositions[deviceId.stringValue] {
            return p as? Position
        }
        return nil
    }
    
    func updateDeviceInfoForPosition(_ info : [String : AnyObject])
    {
        let deviceId : String = (info["id"] as! NSNumber).description
        let position : Position!
        if (self.allPositions.value(forKey: deviceId) == nil)
        {
            position = Position()
            setInfo(info: info, position: position)
            self.allPositions.setValue(position, forKey: deviceId)
        }
        else
        {
            position = self.allPositions.value(forKey: deviceId) as? Position
            setInfo(info: info, position: position)
        }
        
    }
    
    private func setInfo(info : [String : AnyObject], position : Position)
    {
        position.device.id = info["id"] as? NSNumber
        position.device.setDeviceStatus(info["status"] as! String)
        //        position.device.lastUpdate = info["lastUpdate"] as? Date
        position.device.positionId = info["positionId"] as? NSNumber
        position.device.name = info["name"] as? String
        position.device.uniqueId = (info["uniqueId"] as? NSNumber)?.description
    }
    
    func deviceById(_ id: NSNumber) -> Device? {
        if let d = allDevices[id.stringValue] {
            return d as? Device
        }
        return nil
    }
    
    func setPosition(value : Any, key : String)
    {
        allPositions.setValue(value, forKey: key)
    }
    
    func getAllPositions() -> NSMutableDictionary
    {
        return self.allPositions
    }
    
    func fetchDevices(_ onFailure: ((String) -> Void)? = nil, onSuccess: @escaping ([Device]) -> Void) {
        
        let url = "http://iot_test.virginmobile.sa/api/devices"
        
        Alamofire.request(url).responseJSON(completionHandler: { response in
            switch response.result {
                
            case .success(let JSON):
                if response.response!.statusCode != 200 {
                    if let fail = onFailure {
                        fail("Invalid server response")
                    }
                } else {
                    
                    if let data = JSON as? [[String : AnyObject]] {
                        
                        var devices = [Device]()
                        
                        for d in data {
                            let dd = Device()
                            dd.setValuesForKeys(d)
                            devices.append(dd)
                            
                            self.allDevices.setValue(dd, forKey: (dd.id?.stringValue)!)
                        }
                        
                        // tell everyone that the devices have been updated
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Definitions.DeviceUpdateNotificationName), object: nil)
                        
                        onSuccess(devices)
                        
                    } else {
                        if let fail = onFailure {
                            fail("Server response was invalid")
                        }
                    }
                }
                
            case .failure(let error):
                if let fail = onFailure {
                    fail(error.localizedDescription)
                }
            }
        })
    }
    
//    func setupDummyDevices()
//    {
//        let device1 : Device = Device()
//
//        device1.name = "Bosko Smakoski"
//        device1.batteryPercentage = "58%"
//        device1.notifications = 1
//        device1.locationDescription = "Al Ma'refa Street، Ground Floor - Dubai - United Arab Emirates"
//        device1.longitude = 55.1621648
//        device1.lattitude = 25.102984
//        device1.imei = "51061219"
//        device1.lastUpdateAgo = "5 min ago"
//        device1.speedDescription = "3 km/h"
//        device1.status = "Online"
//
//        let device2 : Device = Device()
//
//        device2.name = "Stole Dole"
//        device2.batteryPercentage = "38%"
//        device2.locationDescription = "Abdullah Omran Taryam St - Dubai - United Arab Emirates"
//        device2.longitude = 55.1633128
//        device2.lattitude = 25.1033289
//        device2.imei = "39254722"
//        device2.lastUpdateAgo = "1 min ago"
//        device2.speedDescription = "0 km/h"
//        device2.status = "Online"
//
//
//        let device3 : Device = Device()
//
//        device3.name = "Olga Dolga"
//        device3.batteryPercentage = "25%"
//        device3.locationDescription = "Al Sufouh 2, Dubai Knowledge Village - Dubai - United Arab Emirates"
//        device3.longitude = 55.163319
//        device3.lattitude = 25.102
//        device3.imei = "39254722"
//        device3.lastUpdateAgo = "1 day ago"
//        device3.speedDescription = "0 km/h"
//        device3.status = "offline"
//
//        self.deviceList = [device1, device2, device3]
//    }
    
}
