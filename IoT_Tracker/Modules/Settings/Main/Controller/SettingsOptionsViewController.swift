//
//  SettingsOptionsViewController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/4/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import Alamofire

class SettingsOptionsViewController: BaseViewController {

    @IBOutlet var tableView: UITableView!
    var sectionList : [Setting] = [Setting("DEVICE", ["Device Name"]), Setting("CALLS", ["One Touch Calls", "White List"]), Setting("WORKING MODE", ["Device Mode"])]
    
    
    //    public init(_ nibNameString : String, _ bundle : Bundle?) {
    //        super.init(nibName: nibNameString, bundle: bundle);
    //    }
    //
    //    required public init?(coder aDecoder: NSCoder) {
    //        fatalError("init(coder:) has not been implemented")
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle("Settings Options")
        tableView.register(UINib(nibName: "SettingsOptionsViewCell", bundle: nil), forCellReuseIdentifier: "SettingsOptionsViewCell")
        tableView.tableFooterView = UIView()
        tableView.separatorColor = color.table.separatorColor
    }
}

extension SettingsOptionsViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return sectionList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sectionList[section].categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : SettingsOptionsViewCell = self.tableView.dequeueReusableCell(withIdentifier: "SettingsOptionsViewCell") as! SettingsOptionsViewCell
        cell.categoryNameLabel.text = sectionList[indexPath.section].categories[indexPath.row]
        return cell
    }
    
    
}

extension SettingsOptionsViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int)
    {
        if let header = view as? UITableViewHeaderFooterView {
            header.textLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
            header.textLabel?.textColor = color.table.sectionTextColor
            header.backgroundColor = color.table.sectionBackgroundColor
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return sectionList[section].sectionName
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let selected = sectionList[indexPath.section].categories[indexPath.row]

        if(selected == "Device Name")
        {
            let controller = DeviceNameViewController()
            navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            showLoadingIndicator()
            getDevice(indexPath)
        }
        
    }
    
    func getDevice(_ indexPath : IndexPath)
        {
            let header : HTTPHeaders = [
                "Content-Type" : "application/json",
                "Cookie" : "JSESSIONID=" + UtilityManager.sharedInstance.getCookie()
            ]
    
            let url : String = "http://iot_test.virginmobile.sa/api/devices?id="+(DataManager.sharedInstance.getSelectedPosition()?.device.id?.description)!
    
            Alamofire.request(url, method: .get, parameters: nil, headers: header)
                .responseJSON { response in
                    self.hideLoadingIndicator()
                    if(response.error == nil)
                    {
                        do {
                            let json = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments)
                            let status = ((json as AnyObject).value(forKey: "status") as AnyObject).object(at: 0) as! String
                            if(status == "online")
                            {
                                self.setupForward(true, indexPath)
                            }
                            else
                            {
                                self.setupForward(false, indexPath)
                            }
                        }
                        catch {
                            print("error parsing JSON")
                        }
                    }
                    else
                    {
//                        self.showAlertWithMessage(message: "Device is Offline")
                    }
                    
            }
    
    
        }
    
    func setupForward(_ flag : Bool, _ indexPath : IndexPath)
    {
        let selected = sectionList[indexPath.section].categories[indexPath.row]
        var controller : UIViewController!
        
        if(flag)
        {
            if(selected == "One Touch Calls")
            {
                controller = CallsListViewController()
                (controller as! CallsListViewController).callOption = .oneTouchList
                CommandService.getOneTouchList()
            }
            else if(selected == "White List")
            {
                controller = CallsListViewController()
                (controller as! CallsListViewController).callOption = .whiteList
            }
            else
            {
                controller = DeviceModeViewController()
            }
            navigationController?.pushViewController(controller, animated: true)
        }
        else
        {
            showAlertWithMessage(message: "Sorry the device is offline")
        }
        
    }
    
}

class Setting: NSObject {
    var categories : [String]
    var sectionName : String
    
    init(_ sectionName : String, _ categories : [String]) {
        self.categories = categories
        self.sectionName = sectionName
    }
}
