//
//  SettingsOptionsViewCell.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/4/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

class SettingsOptionsViewCell: UITableViewCell {

    @IBOutlet var categoryNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
