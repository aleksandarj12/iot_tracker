//
//  DeviceNameViewController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/5/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import TPKeyboardAvoidingSwift
import Alamofire
import CoreFoundation

class DeviceNameViewController: BaseViewController {

    @IBOutlet var validationLabel: UILabel!
    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var scrollView: TPKeyboardAvoidingScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitle("Device Name")
        validationLabel.isHidden = true
        self.nameTextField.text = DataManager.sharedInstance.getSelectedPosition()!.device.name
        setupTextField()
        setupButton()
    }
    
    func setupButton()
    {
        confirmButton.setCustomRoundedButton()
    }

    func setupTextField()
    {
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
        nameTextField.leftView = paddingView
        nameTextField.leftViewMode = .always
        nameTextField.layer.cornerRadius = 4.0
        nameTextField.clipsToBounds = true
        nameTextField.layer.borderColor = color.textField.borderColor.cgColor
        nameTextField.layer.borderWidth = 1.0
    }
    
    @IBAction func confirmButtonTapped(_ sender: Any)
    {
        if((nameTextField.text?.contains(" "))!)
        {
            validationLabel.isHidden = false
        }
        else if(nameTextField.text == DataManager.sharedInstance.getSelectedPosition()?.device.name)
        {
            //do nothing
        }
        else
        {
            validationLabel.isHidden = true
            setDeviceName()
        }
    }   
    
    func setDeviceName()
    {
        let device = DataManager.sharedInstance.getSelectedPosition()
        
        if(device?.device.name != nameTextField.text)
        {
            let url = "http://iot_test.virginmobile.sa/api/devices/"+(device?.deviceId?.description)!
            
            let jsonObject: [String: Any] = [
                "attributes": [
                    "id" : "0",
                ],
                "contact" : "",
                "disabled" : false,
                "geofenceIds" : [],
                "groupId" : 0,
                "id" : device?.device.id as! Int,
                "lastUpdate" : device?.device.lastUpdate?.description,
                "model" : "",
                "name" : nameTextField.text!,
                "phone" : "",
                "positionId" : device?.device.positionId,
                "status" : device?.device.getDeviceStatusString(),
                "uniqueId" : device?.device.uniqueId?.description
            ]

            let header : HTTPHeaders = [
                "Content-Type" : "application/json",
                "Cookie" : "JSESSIONID=" + UtilityManager.sharedInstance.getCookie(),
            ]

            let x = SupportFunctions.JSONObjectToJSONString(jsonObject)

            Alamofire.request(url, method: .put, parameters: [:], encoding: x, headers: header) .responseString { response in
                
                let controller = SetDeviceModeViewController()
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalTransitionStyle = .crossDissolve
                controller.mainTitleString = localization.deviceName.settingDeviceName
                controller.option = .deviceName
                if(response.error == nil)
                {
                    DataManager.sharedInstance.setDeviceName(deviceName: self.nameTextField.text!)
                    controller.responseFlag = true
                }
                else
                {
                    controller.responseFlag = false
                }
                self.present(controller, animated: true, completion: nil)
            }
            
        }
        
        
    
    }
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest
    {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}
