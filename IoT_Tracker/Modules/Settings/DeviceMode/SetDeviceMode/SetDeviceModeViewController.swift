//
//  SetDeviceModeViewController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/7/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import GIFProgressHUD

class SetDeviceModeViewController: UIViewController {

    @IBOutlet var responseMainLabel: UILabel!
    @IBOutlet var mainWaitingLabel: UILabel!
    @IBOutlet var contentHolderView: UIView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var responseHolderView: UIView!
    @IBOutlet var doneButton: UIButton!
    var option : CallOption!
    var mainTitleString : String = ""
    var timer = Timer()
    var hud = GIFProgressHUD()
    var responseFlag : Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainWaitingLabel.text = mainTitleString
        self.view.backgroundColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:0.65)
        doneButton.setCustomRoundedButton()
        contentHolderView.setCornerRadiusView(radius: 7.0)
        responseHolderView.setCornerRadiusView(radius: 7.0)
        setupNotification()
        showLoadingOverlay()
        scheduleTimer()
        if(option == .deviceName)
        {
            response(responseFlag!)
        }
    }
    
    @objc func timerAction()
    {
        if(timer.timeInterval == 4)
        {
            timer.invalidate()
            UIView.animate(withDuration: 0.5, animations: {
                self.contentHolderView.alpha = 0.0
                self.responseHolderView.alpha = 1.0
            }) { (flag) in
                self.hideLoadingOverlay()
            }
        }
    }
    
    func scheduleTimer()
    {
        timer.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }

    func showLoadingOverlay()
    {
        hud = GIFProgressHUD.show(withGIFName: "loading", addedTo: contentHolderView, animated: true)
        hud.frame = CGRect(x: (contentHolderView.frame.size.width / 2) - 80, y: (contentHolderView.frame.size.height / 2) - 80, width: 160, height: 160)
        hud.backgroundColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.4)
        hud.containerColor = UIColor.clear
        hud.scaleFactor = 0.5
    }
    
    func hideLoadingOverlay()
    {
        GIFProgressHUD.hide(for: self.view, animated: true)
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(commandResponseSuccess), name: NSNotification.Name(rawValue: keys.commandResponseSuccess), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(commandResponseFailed), name: NSNotification.Name(rawValue: keys.commandResponseFailed), object: nil)
    }
    
    @IBAction func doneButtonTapped(_ sender: Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc func commandResponseSuccess()
    {
        response(true)
    }
    
    @objc func commandResponseFailed()
    {
        response(false)
    }
    
    func response(_ flag : Bool)
    {
        responseMainLabel.text = flag ? localization.successfulActionTitle : localization.failedActionTitle
        if(option == .oneTouchList)
        {
            messageLabel.text = flag ? localization.oneTouchCall.successfulOneTouchCall : localization.oneTouchCall.failedOneTouchCall
        }
        else if(option == .whiteList)
        {
            messageLabel.text = flag ? localization.whiteList.successfulWhiteList : localization.whiteList.failedWhitelist
        }
        else if(option == .deviceMode)
        {
            messageLabel.text = flag ? localization.deviceMode.successfulDeviceMode : localization.deviceMode.failedDeviceMode
        }
        else
        {
            messageLabel.text = flag ? localization.deviceName.successfulDeviceName : localization.deviceName.failedDeviceName
        }
        
        scheduleTimer()
        //tuka treba da se smene setting up device mode.... ekranot
    }
}
