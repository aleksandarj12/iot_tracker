//
//  DeviceModeViewController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/6/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

enum SelectedMode {
    case defaultMode
    case vibrationMode
}

class DeviceModeViewController: BaseViewController {

    @IBOutlet var tableView: UITableView!
    var selectedMode : SelectedMode = .vibrationMode
    var fullNumber : Int?
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitle("Device Mode")
        setupTableView()
        setupFooterView()
        showLoadingIndicator()
        CommandService.getDeviceMode()
        setupNotification()
    }

    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(successGetResponse), name: NSNotification.Name(rawValue: keys.successGetResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateEvents), name: NSNotification.Name(rawValue: keys.updateEvents), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(commandResponseSuccess), name: NSNotification.Name(rawValue: keys.commandResponseSuccess), object: nil)
    }
    
    @objc func commandResponseSuccess()
    {
        print("tuka")
    }
    
    @objc func successGetResponse()
    {
        scheduleTimer()
    }
    
    @objc func updateEvents()
    {
        if(DataManager.sharedInstance.getDeviceMode() == .defaultMode)
        {
            if let number = fullNumber {
                DataManager.sharedInstance.setTimeInterval(seconds: number * 60)
            }
            selectedMode = .defaultMode
        }
        else
        {
            selectedMode = .vibrationMode
        }
        
        hideLoadingIndicator()
        tableView.reloadData()
    }
    
    func scheduleTimer()
    {
        timer.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        if(timer.timeInterval == 15)
        {
            timer.invalidate()
            hideLoadingIndicator()
        }
    }
    
    func setupFooterView()
    {
        let view : UIView = CallFooterView.instanceFromNib()
        (view as! CallFooterView).delegate = self
        (view as! CallFooterView).additionalHolderView.isHidden = true
        (view as! CallFooterView).frame.size.height -= 52
        
        self.tableView.tableFooterView = view
    }
    
    func setupTableView()
    {
        tableView.register(UINib(nibName: "ModeViewCell", bundle: nil), forCellReuseIdentifier: "ModeViewCell")
        tableView.register(UINib(nibName: "CallHeaderViewCell", bundle: nil), forCellReuseIdentifier: "CallHeaderViewCell")
        tableView.register(UINib(nibName: "TipsViewCell", bundle: nil), forCellReuseIdentifier: "TipsViewCell")
        tableView.register(UINib(nibName: "TimeIntervalViewCell", bundle: nil), forCellReuseIdentifier: "TimeIntervalViewCell")
    }
   

}

extension DeviceModeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell : CallHeaderViewCell = self.tableView.dequeueReusableCell(withIdentifier: "CallHeaderViewCell") as! CallHeaderViewCell
            
            cell.titleLabel.text = "Set Device Mode"
            cell.detailsLabel.text = "You can define working mode and device will report its location based on your settings."
            
            return cell
        }
            
        else if(indexPath.row == 3)
        {
            let cell : TipsViewCell = self.tableView.dequeueReusableCell(withIdentifier: "TipsViewCell") as! TipsViewCell
            cell.setupTips(selectedMode)
            
            return cell
        }
            
        else if(indexPath.row == 5)
        {
            let cell : TipsViewCell = self.tableView.dequeueReusableCell(withIdentifier: "TipsViewCell") as! TipsViewCell
            cell.setupTimeIntervalTips()
            if(selectedMode == .defaultMode)
            {
                cell.contentView.isHidden = false
            }
            else
            {
                cell.contentView.isHidden = true
            }
            
            return cell
        }
        else if(indexPath.row == 4)
        {
            let cell : TimeIntervalViewCell = self.tableView.dequeueReusableCell(withIdentifier: "TimeIntervalViewCell") as! TimeIntervalViewCell
            cell.delegate = self
            cell.setTimeInterval()
            
                if(selectedMode == .vibrationMode)
                {
                    cell.contentView.isHidden = true
                }
                else
                {
                    cell.contentView.isHidden = false
                }
            
            return cell
        }
        else
        {
            let cell : ModeViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ModeViewCell") as! ModeViewCell
            cell.setupMode(indexPath.row ,selectedMode)
            return cell
        }
    }
}

extension DeviceModeViewController : TimeIntervalCellDelegate {
    func didChangeSliderValue(value: Int?) {
        self.fullNumber = value
    }
    
    
}

extension DeviceModeViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.row == 1 || indexPath.row == 2)
        {
            return 60.0
        }
        else if(indexPath.row == 4)
        {
            return 132.0
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(tableView.cellForRow(at: indexPath) is ModeViewCell)
        {
            let cell : ModeViewCell = tableView.cellForRow(at: indexPath) as! ModeViewCell
            if(cell.modeLabel.text == "Default Mode")
            {
                selectedMode = .defaultMode
            }
            else if(cell.modeLabel.text == "Vibration Mode")
            {
                selectedMode = .vibrationMode
            }
            tableView.reloadData()
        }
    }
}

extension DeviceModeViewController : CallFooterDelegate {
    func didSelectConfirmButton() {
        
        let controller = SetDeviceModeViewController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.modalTransitionStyle = .crossDissolve
        controller.mainTitleString = localization.deviceMode.settingDeviceMode
        
        if(selectedMode == .defaultMode)
        {
            CommandService.setDeviceMode(minutes: fullNumber)
            DataManager.sharedInstance.setLocalDeviceMode(seconds: fullNumber! * 60)
        }
        else
        {
            CommandService.setDeviceMode(minutes: nil)
            DataManager.sharedInstance.setLocalDeviceMode(seconds: 30)
        }

        present(controller, animated: true, completion: nil)
    }
    
    func didSelectAdditionalNumberButton() {
        
    }
    
    
}
