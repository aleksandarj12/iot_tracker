//
//  TimeIntervalViewCell.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/6/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

protocol TimeIntervalCellDelegate {
    func didChangeSliderValue(value : Int?)
}

class TimeIntervalViewCell: UITableViewCell {
    
    @IBOutlet var timeSlider: CustomSlider!
    @IBOutlet var minutesLabel: UILabel!
    var delegate : TimeIntervalCellDelegate!
    var numbers : [Int] = [Int]()
    override func awakeFromNib() {
        super.awakeFromNib()
        minutesLabel.text = Int(timeSlider.value).description + " minutes"
        timeSlider.bounds = timeSlider.trackRect(forBounds: timeSlider.bounds)
        numbers = setNumbers()
        
    }
    
    func didChangeValue(value: Int)
    {
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func onChangeSlider(_ sender: UISlider)
    {
        let fullNumber = Int(sender.value)
        delegate.didChangeSliderValue(value: fullNumber)
        minutesLabel.text = fullNumber.description + " minutes"
//        minutesLabel.text = numbers[fullNumber-1].description + " minutes"
    }
    
    func setTimeInterval()
    {
        let minutes = DataManager.sharedInstance.getTimeInterval() / 60
        timeSlider.value = Float(minutes)
        minutesLabel.text = minutes.description + " minutes"
    }
    
    func setNumbers() -> [Int]
    {
        var numbers : [Int] = [1,2,3,4,5,6,7,8,9]
        
        var increment : Int = 10
        
        while(increment != 1440)
        {
            numbers.append(increment)
            increment += 10
        }
        numbers.append(increment)
        
        return numbers
    }
    
}
