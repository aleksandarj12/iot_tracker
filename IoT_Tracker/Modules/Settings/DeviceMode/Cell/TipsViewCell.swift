//
//  TipsViewCell.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/6/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

class TipsViewCell: UITableViewCell {

    @IBOutlet var modeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupTimeIntervalTips()
    {
        modeLabel.text = "Reporting in regular time interval every 180 seconds. You can reduce this to min 1 minute or set to max of 1440 minutes."
    }
    
    func setupTips(_ mode : SelectedMode)
    {
        if(mode == .defaultMode)
        {
            modeLabel.text = "When Default Mode is set, device will report its location on fixed time interval."
        }
        else
        {
            modeLabel.text = "When Vibration Mode is set, device will report its location only when movement is sensed every 30 seconds."
        }
    }
    
}
