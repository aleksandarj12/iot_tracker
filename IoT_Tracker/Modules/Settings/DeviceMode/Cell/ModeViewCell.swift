//
//  ModeViewCell.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/6/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

class ModeViewCell: UITableViewCell {

    @IBOutlet var modeLabel: UILabel!
    @IBOutlet var checkImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMode(_ row : Int, _ mode : SelectedMode)
    {
        if(row == 1)
        {
            modeLabel.text = "Default Mode"
            if(mode == .defaultMode)
            {
                checkImageView.isHidden = false
            }
            else
            {
                checkImageView.isHidden = true
            }
        }
        else if(row == 2)
        {
            modeLabel.text = "Vibration Mode"
            if(mode == .vibrationMode)
            {
                checkImageView.isHidden = false
            }
            else
            {
                checkImageView.isHidden = true
            }
        }
        
        
       
    }
    
}
