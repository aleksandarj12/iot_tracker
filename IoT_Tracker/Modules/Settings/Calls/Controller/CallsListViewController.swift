//
//  CallsListViewController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/5/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

enum CallOption {
    case whiteList
    case oneTouchList
    case deviceMode
    case deviceName
}
class CallsListViewController: BaseViewController {

    @IBOutlet var tableView: UITableView!
    var phoneNumbers : [PhoneNumber] = [PhoneNumber]()
    var callOption : CallOption!
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        setupFooterView()
        setupNotification()
        
        showLoadingIndicator()
        if(callOption == .whiteList)
        {
            setTitle("White List")
            CommandService.getWhiteList()
        }
        else if(callOption == .oneTouchList)
        {
            CommandService.getOneTouchList()
            setTitle("One Touch Calls")
        }
    }
    
    func setupNotification()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(successGetResponse), name: NSNotification.Name(rawValue: keys.successGetResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(failedGetResponse), name: NSNotification.Name(rawValue: keys.failedGetResponse), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateEvents), name: NSNotification.Name(rawValue: keys.updateEvents), object: nil)
    }
    
    @objc func successGetResponse()
    {
        scheduleTimer()
    }
    
    @objc func updateEvents()
    {
        timer.invalidate()
        hideLoadingIndicator()
        if(callOption == .oneTouchList)
        {
            phoneNumbers = DataManager.sharedInstance.oneTouchCall
        }
        else if(callOption == .whiteList)
        {
            phoneNumbers = DataManager.sharedInstance.whiteList
        }
        
        if(phoneNumbers.count == 0)
        {
            phoneNumbers.append(PhoneNumber(""))
        }
        tableView.reloadData()
        hideLoadingIndicator()
    }
    
    func scheduleTimer()
    {
        timer.invalidate()
        
        timer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        if(timer.timeInterval == 15)
        {
            timer.invalidate()
            hideLoadingIndicator()
        }
    }
    
    @objc func failedGetResponse()
    {
//        hideLoadingIndicator()
        showAlertWithMessage(message: "Problem loading numbers!")
    }
    
    func setupFooterView()
    {
        let view : UIView = CallFooterView.instanceFromNib()
        (view as! CallFooterView).delegate = self
        self.tableView.tableFooterView = view
    }
    
    func setupTableView()
    {
        tableView.register(UINib(nibName: "CallDetailsViewCell", bundle: nil), forCellReuseIdentifier: "CallDetailsViewCell")
        tableView.register(UINib(nibName: "CallHeaderViewCell", bundle: nil), forCellReuseIdentifier: "CallHeaderViewCell")
    }
}

extension CallsListViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(section == 0)
        {
            return 1
        }
        else
        {
            return phoneNumbers.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell()
        
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell : CallHeaderViewCell = self.tableView.dequeueReusableCell(withIdentifier: "CallHeaderViewCell") as! CallHeaderViewCell
                cell.setupHeader(callOption)
                
                return cell
                
            default:
                return cell
            }
        case 1:
            let cell : CallDetailsViewCell = self.tableView.dequeueReusableCell(withIdentifier: "CallDetailsViewCell") as! CallDetailsViewCell
            cell.delegate = self
            cell.setupCell(row: indexPath.row, numberOfPhones: phoneNumbers.count)
            cell.setupPhone(phoneNumbers[indexPath.row])
            return cell
        default:
            return cell
        }
    }
}

extension CallsListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
}

extension CallsListViewController : CallDetailsDelegate {
    
    func didRemovePhoneNumber(_ cell: UITableViewCell)
    {
        let indexPath : IndexPath = tableView.indexPath(for: cell)!
        phoneNumbers.remove(at: indexPath.row)
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .automatic)
        tableView.reloadData()
        tableView.endUpdates()
    }
    
    func didChangedPhoneNumber(_ cell: UITableViewCell)
    {
        let row = tableView.indexPath(for: cell)?.row
        phoneNumbers[row!].number = (cell as! CallDetailsViewCell).phoneTextField.text!
    }
}

extension CallsListViewController : CallFooterDelegate {
    
    func didSelectAdditionalNumberButton()
    {
        if(callOption == .whiteList)
        {
            if(phoneNumbers.count < 15)
            {
                addNewRow()
            }
            else
            {
                showAlertWithMessage(message: "You have exceed the limit of numbers")
            }
        }
        else if(callOption == .oneTouchList)
        {
            if(phoneNumbers.count < 3)
            {
                addNewRow()
            }
            else
            {
                showAlertWithMessage(message: "You have exceed the limit of numbers")
            }
        }
    }
    
    func addNewRow()
    {
        let indexPath : IndexPath = IndexPath(row: phoneNumbers.count, section: 1)
        let phoneNumber : PhoneNumber = PhoneNumber("")
        phoneNumbers.append(phoneNumber)
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.reloadData()
        tableView.endUpdates()
    }
    
    func didSelectConfirmButton()
    {
        var flag = true
        for number in phoneNumbers
        {
            if(ValidationControl.validateNumber(number: number.number))
            {
                number.valid = true
            }
            else
            {
                number.valid = false
                flag = false
            }
        }
        
        if(flag)
        {
            let controller = SetDeviceModeViewController()
            controller.modalPresentationStyle = .overCurrentContext
            controller.modalTransitionStyle = .crossDissolve
            
            if(callOption == .whiteList)
            {
                controller.mainTitleString = localization.whiteList.settingWhiteList
                controller.option = .whiteList
                CommandService.setWhiteListNumbers(phoneNumbers)
            }
            else if(callOption == .oneTouchList)
            {
                controller.mainTitleString = localization.oneTouchCall.settingOneTouchCall
                controller.option = .oneTouchList
                CommandService.setOneTouchCall(phoneNumbers)
            }
            else
            {
                
            }
            present(controller, animated: true, completion: nil)
        }
        else
        {
            tableView.reloadData()
        }
    }
    
}

class PhoneNumber: NSObject {
    var number : String
    var valid : Bool = true
    
    init(_ number : String) {
        self.number = number
    }
}
