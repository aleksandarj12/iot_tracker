//
//  CallFooterView.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/5/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

protocol CallFooterDelegate {
    func didSelectConfirmButton()
    func didSelectAdditionalNumberButton()
}

class CallFooterView: UIView {
        
    @IBOutlet var confirmHolderView: UIView!
    @IBOutlet var additionalHolderView: UIView!
    
    @IBOutlet var additionalNumberButton: UIButton!
    @IBOutlet var confirmButton: UIButton!
    var delegate : CallFooterDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        confirmButton.setCustomRoundedButton()
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CallFooterView", bundle: nil).instantiate(withOwner: nil, options: nil).last as! UIView
        }
        
    

    @IBAction func additionalNumberButtonTapped(_ sender: Any)
    {
        delegate.didSelectAdditionalNumberButton()
    }
    
    @IBAction func confirmButtonTapped(_ sender: Any)
    {
        delegate.didSelectConfirmButton()
    }
}

extension UIButton {
    
    func setCustomRoundedButton() {
        self.layer.cornerRadius = 25.5
        self.clipsToBounds = true
        self.backgroundColor = color.primaryMainRed
        self.titleLabel?.textColor = UIColor.white
    }
}
