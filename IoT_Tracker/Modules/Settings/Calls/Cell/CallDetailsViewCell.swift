//
//  CallDetailsViewCell.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/5/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

enum CellTypePosition {
    case first
    case last
    case clone
}

protocol CallDetailsDelegate {
    func didChangedPhoneNumber(_ cell : UITableViewCell)
    func didRemovePhoneNumber(_ cell : UITableViewCell)
}

class CallDetailsViewCell: UITableViewCell {

    @IBOutlet var validationErrorLabel: UILabel!
    @IBOutlet var removeTextButton: UIButton!
    @IBOutlet var removeButton: UIButton!
    @IBOutlet var detailsHolderView: UIView!
    @IBOutlet var tipsHolderView: UIView!
    @IBOutlet var phoneTextField: UITextField!
    @IBOutlet var contentHolderView: UIView!
    
//    @IBOutlet var separatorHolderView: UIView!
    var delegate : CallDetailsDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentHolderView.layer.borderColor = color.textField.borderColor.cgColor
        
        contentHolderView.layer.borderWidth = 1.0
        contentHolderView.setCornerRadiusView(radius: 7.0)
        phoneTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupPhone(_ phone : PhoneNumber)
    {
        phoneTextField.text = phone.number
        if(phone.valid)
        {
            validationErrorLabel.isHidden = true
        }
        else
        {
            if(detailsHolderView.isHidden)
            {
                detailsHolderView.isHidden = false
            }
            validationErrorLabel.isHidden = false
        }
    }
    
    func setupCell(row : Int, numberOfPhones : Int)
    {
//        separatorHolderView.isHidden = true
        if(row == 0 && numberOfPhones == 1)
        {
            detailsHolderView.isHidden = true
            removeButton.isHidden = true
            tipsHolderView.isHidden = false
        }
        else if(row == numberOfPhones - 1 && numberOfPhones != 1)
        {
            detailsHolderView.isHidden = false
            tipsHolderView.isHidden = false
        }
        else if(row == 0)
        {
            detailsHolderView.isHidden = false
            tipsHolderView.isHidden = true
            removeButton.isHidden = true
//            separatorHolderView.isHidden = false
        }
        else
        {
            detailsHolderView.isHidden = false
            tipsHolderView.isHidden = true
        }
    }
    
    @IBAction func removeTextButtonTapped(_ sender: Any)
    {
        
    }
    
    @IBAction func removeNumberButtonTapped(_ sender: Any)
    {
        delegate.didRemovePhoneNumber(self)
    }
}

extension CallDetailsViewCell : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        textField.text = ValidationControl.checkForStringParams(textField.text!, string)
        delegate.didChangedPhoneNumber(self)
        return false
    }
}
