//
//  CallHeaderViewCell.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/5/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

class CallHeaderViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var detailsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupHeader(_ option : CallOption)
    {
        switch option {
        case .oneTouchList:
            titleLabel.text = "Set One Touch Call"
            detailsLabel.text = "By defining one touch call, the device will be able to call you anytime. You can set up to 3 numbers."
            break
            
        case .whiteList:
            titleLabel.text = "Set White List"
            detailsLabel.text = "By defining white list of numbers the device will be able to receive calls only from these numbers.\n You can set up to 15 numbers."
            break
        case .deviceMode:
            break
        case .deviceName:
            break
        }
    }
}
