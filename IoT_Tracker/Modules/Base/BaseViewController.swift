//
//  BaseViewController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 11/22/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupNavigation()
    }
    

    func showLoadingIndicator()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideLoadingIndicator()
    {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func showAlertWithMessage(message : String)
    {
        let controller : UIAlertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(controller, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return UIStatusBarStyle.lightContent
    }

    func setupNavigation()
    {
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(backButtonTapped))
        

        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font : font.systemFontRegular, NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = color.primaryMainRed
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    @objc func backButtonTapped()
    {
        navigationController?.popViewController(animated: true)
    }
    
    func setTitle(_ title : String)
    {
        self.navigationItem.title = title
    }
}
