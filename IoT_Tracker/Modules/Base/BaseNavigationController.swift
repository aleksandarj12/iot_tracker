//
//  BaseNavigationController.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 11/22/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return UIStatusBarStyle.lightContent
    }

}
