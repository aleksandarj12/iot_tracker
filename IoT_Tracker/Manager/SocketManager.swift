//
//  SocketManager.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/12/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import SocketRocket

class SocketManager: NSObject, SRWebSocketDelegate {

    static let sharedInstance : SocketManager = {
        let instance = SocketManager()
        return instance
    }()
    
    var webSocket : SRWebSocket!

    func setupWebSocket()
    {
        closeSocket()
        reconnectSocket()
    }
    
    //MARK: SOCKET ROCKET
    
    func closeSocket()
    {
        if let s = webSocket {
            s.close()
            s.delegate = nil
            webSocket = nil
        }
    }
    
    func reconnectSocket()
    {
        let serverURL = "http://iot_test.virginmobile.sa/api/socket"
        var request : URLRequest = URLRequest(url: URL(string: serverURL)!)
        request.timeoutInterval = 5
        request.setValue("JSESSIONID="+UtilityManager.sharedInstance.getCookie(), forHTTPHeaderField: "Cookie")
        
        webSocket = SRWebSocket(urlRequest: request)
        webSocket.delegate = self
        webSocket.open()
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!)
    {
        if let data = (message as! String).data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                
                if let p = json["positions"] as? [[String: AnyObject]] {
                    parsePositionData(p)
                }
                else if let d = json["devices"] as? [[String: AnyObject]] {
                    parseDestinationData(d)
                }
                else if let e = json["events"] as? [[String : AnyObject]] {
                    parseEvents(e)
                }
            }
            catch {
                print("error parsing JSON")
            }
        }
    }
    
    func parseDestinationData(_ data: [[String: AnyObject]])
    {
        for d in data {
            DataManager.sharedInstance.updateDeviceInfoForPosition(d)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: keys.updateDeviceInfoForIdNotification), object: self, userInfo: ["id" : d["id"] as! NSNumber])
        }
    }
    
    fileprivate func parseEvents(_ data : [[String : AnyObject]])
    {
        for d in data {
            DataManager.sharedInstance.updateEventsForDevice(d)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: keys.updateEventsInfoForIdNotification), object: self, userInfo: ["id" : d["id"] as! NSNumber])
        }
    }
    
    fileprivate func parsePositionData(_ data: [[String : AnyObject]]) {
        
        var positions = [Position]()
        
        for p in data {
            if let pos = DataManager.sharedInstance.getAllPositions().value(forKey: ((p["deviceId"] as? NSNumber)?.description)!) {
                (pos as? Position)?.setValuesForKeys(p)
            }
            else
            {
                let pp = Position()
                pp.setValuesForKeys(p)
                positions.append(pp)
                DataManager.sharedInstance.setPosition(value: pp, key: (pp.deviceId?.stringValue)!)
            }
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: keys.setPositionsNotificationCenter), object: nil)
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: Error!)
    {
        reconnectSocket()
        print(error.localizedDescription)
    }
}
