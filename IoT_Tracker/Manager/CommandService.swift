//
//  CommandService.swift
//  IoT_Tracker
//
//  Created by Aleksandar Jovanov on 12/11/18.
//  Copyright © 2018 3P Development. All rights reserved.
//

import UIKit
import Alamofire

enum CommandOption {
    case get
    case set
}
class CommandService: NSObject {
    
//   class func setWhiteListNumber()
//    {
//        id: integer
//        deviceId: integer
//        description: string
//        type: string
//        attributes: object
//    }
    
    class func sendCommand(_ command : String, _ commandOption : CommandOption, _ callOption : CallOption)
    {
        let url = "http://iot_test.virginmobile.sa/api/commands/send"
        
        let jsonObject: [String: Any] = [
            "attributes": [
                "data" : command,
            ],
            "id" : 0,
            "deviceId" : Int(truncating: (DataManager.sharedInstance.getSelectedPosition()?.device.id)!),
            "description" : "New…",
            "type" : "custom"
        ]
        let header : HTTPHeaders = [
            "Content-Type" : "application/json",
            "Cookie" : "JSESSIONID=" + UtilityManager.sharedInstance.getCookie(),
            ]
        
        let x = SupportFunctions.JSONObjectToJSONString(jsonObject)
        
        Alamofire.request(url, method: .post, parameters: [:], encoding: x, headers: header) .responseString { response in
            
            if(response.error == nil)
            {
                if(commandOption == .get)
                {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: keys.successGetResponse), object: self)
                }
                else
                {
                    if(callOption == .deviceMode)
                    {
                        DataManager.sharedInstance.setTimeInterval(seconds: DataManager.sharedInstance.localDeviceMode!)
                    }
//                    DataManager.sharedInstance.setDeviceMode
                    NotificationCenter.default.post(name: Notification.Name(rawValue: keys.commandResponseSuccess), object: self)
                }
            }
            else
            {
                if(commandOption == .get)
                {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: keys.failedGetResponse), object: self)
                }
                else
                {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: keys.commandResponseFailed), object: self)
                }
            }
        }
    }
    
    class func getDeviceMode()
    {
        sendCommand("MODE#", .get, .deviceMode)
    }
    
    class func setDeviceMode(minutes : Int?)
    {
        if let m = minutes
        {
            let seconds = m * 60
            sendCommand("MODE,1,"+seconds.description+"#", .set, .deviceMode)
        }
        else
        {
            sendCommand("MODE,2,30#", .set, .deviceMode)
        }
        
    }
    
    class func deleteOneTouch(number : String)
    {
        sendCommand("SOS,D,"+number, .set, .oneTouchList)
    }
    
    class func deleteWhiteList()
    {
        
    }
    
    class func getOneTouchList()
    {
        sendCommand("SOS#", .get, .oneTouchList)
    }
    
    class func getWhiteList()
    {
        sendCommand("WN#", .get, .whiteList)
    }
    
    class func setOneTouchCall(_ numbers : [PhoneNumber]?)
    {
        sendCommand("SOS,A,"+SupportFunctions.removeSpacePhoneNumbers(numbers!, .oneTouchList), .set, .oneTouchList)
    }
    
    class func setWhiteListNumbers(_ numbers : [PhoneNumber]?)
    {
        sendCommand("WN,A,"+SupportFunctions.removeSpacePhoneNumbers(numbers!, .whiteList), .set, .whiteList)
    }        
    
    
    
}
