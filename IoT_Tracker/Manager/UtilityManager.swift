//
//  UtilityManager.swift
//  ScrollSwipe
//
//  Created by Aleksandar Jovanov on 11/27/18.
//  Copyright © 2018 Aleksandar Jovanov. All rights reserved.
//

import UIKit

class UtilityManager: NSObject {
    var window : UIWindow!
    var cookie : String!
    static let sharedInstance : UtilityManager = {
        let instance = UtilityManager()
        return instance
    }()
    
    func setCookie(_ cookie : String)
    {
        self.cookie = cookie
    }
    
    func getCookie() -> String
    {
        return self.cookie
    }
    
    func setMainWindow(_ window : UIWindow)
    {
        self.window = window
    }
    
    func setStatusBarBackgroundColor(_ alpha: CGFloat) {
        
        let color : UIColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha: alpha)
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        
        statusBar.backgroundColor = color
    }
    
    func changeStatusBarBackground(_ alpha : CGFloat)
    {
        let color : UIColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha: 1.0)
        //        self.window?.windowLevel = .statusBar
        //        self.window?.backgroundColor = UIColor.green
        self.window.layer.backgroundColor = UIColor.green.cgColor
        
        
        
        //        UIView.animate(withDuration: 0.5)
        //        {
        //        }
        
    }
    
}
